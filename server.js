var express  = require('express'),
cookieParser = require('cookie-parser'),
bodyParser   = require('body-parser'),
session      = require('express-session'),
mongoose     = require('mongoose'),
io           = require('socket.io'),
fs           = require('fs'),
config       = require('./config'),
app          = express();

// Bootstrap db connection
var db = mongoose.connect(config.db, { auto_reconnect: true });

// Sessions
app.use(bodyParser());
app.use(cookieParser());
app.use(session({
    secret: config.sessionSecret,
    cookie: { maxAge: 1000 * 60 * 60 * 6 }
}));

// Static
app.use(express.static(__dirname + '/public'));

// Render engine
app.engine('.html', require('ejs').__express);
app.set('views', __dirname + '/app/views');
app.set('view engine', 'html');

var walk = function(path, _app) {
  fs.readdirSync(path).forEach(function(file) {
    var newPath = path + '/' + file;
    var stat    = fs.statSync(newPath);
    if (stat.isFile()) {
      if (/(.*)\.(js$|coffee$)/.test(file)) {
        if(_app){
          require(newPath)(_app);
        } else {
          require(newPath);
        }
      }
    } else if (stat.isDirectory()) {
      walk(newPath);
    }
  });
};

// Models
var models_path = __dirname + '/app/models';
walk(models_path, null);

// Routes
require('./app/routes')(app);

var server = app.listen(config.port);
var sockets = io.listen(server);

var users = {};
sockets.on('connection', function (socket){
  users[socket.id] = { socket: socket };
  require('./app/socket_events')(socket, users);
});

console.log('Alloy Studio Server Running...');