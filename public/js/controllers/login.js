app.controller('loginController', ['$scope', '$http', function($scope, $http){
    $scope.requestInvite = false;
    $scope.loading       = false;

    $scope.login = function() {
        $scope.loading = true;
        $http.post('/api/users/signin', { username: $scope.username, password: $scope.password })
        .then(function(response){
            $scope.loading = false;
            response = response.data;

            if(response.success){
                location.reload();
            } else {
                alert(response.error);
            }
        });
    };

    $scope.request = function() {
        $scope.loading = true;
        $http.post('/api/users/request', { email: $scope.email }).then(function(data){
            $scope.loading       = false;
            $scope.email         = '';
            $scope.requestInvite = false;

            alert('Your request has been sent!');
        });
    };

}]);