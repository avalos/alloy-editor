app.controller('editorController', ['$scope', '$rootScope', '$http', function($scope, $rootScope, $http){

    var updatedProject = false;

    $scope.rootUrl = location.protocol + '//' + location.host;
    $scope.user    = user;
    $scope.chats   = [];

    $scope.viewTypes = [
        'Window',
        'View',
        'Button',
        'ImageView',
        'TableView',
        'TableViewRow'
    ];

    $scope.devices = [
        {
            name: 'iPhone 4',
            resolution: {
                width: '640px',
                height: '960px'
            },
            dpi: 326,
            zoom: 0.6
        },
        {
            name: 'iPhone 5',
            resolution: {
                width: '640px',
                height: '1136px'
            },
            dpi: 326,
            zoom: 0.6
        },
        {
            name: 'iPad / iPad Mini (1st Gen)',
            resolution: {
                width: '1024px',
                height: '768px'
            },
            dpi: 132,
            zoom: 0.49
        },
        {
            name: 'iPad Mini (Retina)',
            resolution: {
                width: '2048px',
                height: '1536px'
            },
            dpi: 326,
            zoom: 0.25
        }
    ];

    $scope.styles = {
        backgroundColor: 'background-color',
        image          : 'background-image',
        top            : 'margin-top',
        bottom         : 'margin-bottom',
        left           : 'margin-left',
        right          : 'margin-right',
        backgroundImage: 'background-image',
        borderWidth    : 'border',
        borderColor    : 'border-color'
    };

    $scope.tiValues = {
        'Ti.UI.SIZE': 'auto',
        'Ti.UI.FILL': '100%'
    };

    $addChildValue = '';

    $scope.tss = {};

    $scope.project = {
        name: '',
        controllers: [
            {
                name: 'index',
                selected: true,
                views   : [
                    {
                        id: 'main',
                        type: 'Window',
                        selected: true,
                        properties: [{
                            name: 'backgroundColor',
                            value: '#fff'
                        }]
                    }
                ]
            }
        ]
    };

    $scope.new = {};

    $rootScope.$editorScope = $scope;

    $scope.getSelectedDevice = function() {
        var device = _.find($scope.devices, function(device){
            return device.selected;
        });
        return device || $scope.devices[0];
    };

    $scope.selectDevice = function(device) {
        _.each($scope.devices, function(device){
            device.selected = false;
        });
        device.selected = true;
    };

    $scope.getSelectedController = function(){
        var controller = _.find($scope.project.controllers, function(controller){
            return controller.selected;
        }) || $scope.project.controllers[0];
        return controller;
    };

    $scope.selectController = function(controller) {
        _.each($scope.project.controllers, function(controller){
            controller.selected = false;
        });
        controller.selected = true;
    };

    $scope.removeProp = function(property){
        var selectedView = $scope.getSelectedView();
        selectedView.properties.splice(_.indexOf(selectedView.properties, property), 1);
    };

    $scope.removeController = function(controller){
        var _confirm = confirm('Do you want to remove the '+ controller.name +' controller?');
        if(_confirm){
            $scope.project.controllers.splice(_.indexOf($scope.project.controllers, controller), 1);
            $scope.selectController($scope.project.controllers[0]);
        }
    };

    $rootScope.getStyle = $scope.getStyle = function(view){
        var style = { position: 'relative' };

        view.tss = {
            value: {}
        };

        if(view.id){
            view.tss.id = '#' + view.id;
        }

        _.each(view.properties, function(property){
            if(property.value && property.name){
                var property_name = property.name;

                if(property_name.toLowerCase() === 'width' && _.isNumber(property.value)){
                    style['width'] = property.value * ($scope.getSelectedDevice().dpi / 160);
                } else if(property_name.toLowerCase() === 'height' && _.isNumber(property.value)) {
                    style['height'] = property.value * ($scope.getSelectedDevice().dpi / 160);
                } else if (property_name.toLowerCase() === 'borderWidth') {
                    style['border'] = 'solid ' + property.value + ' #000';
                } else if (property_name.toLowerCase() === 'image' || property_name === 'backgroundImage') { // TODO (Support for ImageViews - src attr)
                    style['background-image']  = "url('" + property.value + "')";
                } else {
                    style[$scope.styles[property_name] || property_name] = $scope.tiValues[property.value] || property.value;
                }
            }

            if(view.id){
                view.tss.value[property_name] = property.value;
            }
        });

        if(view.tss.id){
            var tss_file = '"' + view.tss.id + '": ' + JSON.stringify(view.tss.value);
        }

        if(view.type === 'View' || view.type === 'Window' || view.type === 'TableView'){
            if(!style.width){
                style.width = '100%';
            }
            if(!style.height){
                style.height = '100%';
            }
        }

        if(view.selected){
            style.border = 'solid 1px #008cba';
        }
        return style;
    };

    var unSelectViews = function(views){
        _.each(views, function(view){
            delete view.selected;
            if(view.views){
                unSelectViews(view.views);
            }
        });
    };

    $scope.selectView = function(view, $event){
        if($event){
            var src = $($event.originalEvent.srcElement);
            // if(src.is('div')){
                $event.stopPropagation();
            // }
        }
        unSelectViews($scope.getSelectedController().views);
        view.selected = true;
    };

    $scope.showAddModal = function(){
        $scope.addChildValue = '';
        $('#add-child-modal').modal('show');
    };

    $scope.removeView = function(view){
        if(view.parent_id){
            var parent = getView(view.parent_id, $scope.getSelectedController().views);
            if(parent){
                parent.views.splice(_.indexOf(parent.views, view), 1);
            } else {
                console.error('Parent not found', view);
            }
        }
    };

    $scope.addChild = function(){
        var selectedView = $scope.getSelectedView();

        if(!selectedView.views){
            selectedView.views = [];
        }

        var new_view = {
            element_id: new Date().getTime(),
            parent_id : selectedView.element_id,
            type      : $scope.addChildValue,
            properties: [{}]
        };

        if($scope.addChildValue === 'Button'){
            new_view.properties[0].name = 'title';
            new_view.properties[0].value = 'Button';
        }

        selectedView.views.push(new_view);
        $scope.selectView(new_view);
        $('#add-child-modal').modal('hide');
    };

    var getView = $rootScope.getView = function(id, views){
        views = views || $scope.getSelectedController().views;
        var _view;
        _.each(views, function(view){
            if(view.element_id == id){
                _view = view;
            } else {
                if(view.views){
                    _view = getView(id, view.views);
                }
            }
        });
        return _view;
    };

    var searchSelected = function(views){
        var selected;
        _.each(views, function(view){
            if(view.selected){
                selected = view;
            } else {
                if(view.views){
                    selected = searchSelected(view.views);
                }
            }
        });
        return selected;
    };

    $scope.getSelectedView = function(){
        var view = searchSelected($scope.getSelectedController().views);
        return view || {};
    };

    var getTags = function(views, spaces){
        var tags = '';
        _.each(views, function(view){
            var properties = '';
            if(view.id){
                properties += ' id="' + view.id + '"';
            } else {
                _.each(view.properties, function(property){
                    if(property.value){
                        properties += ' ' + property.name + '="' + property.value + '"';
                    }
                });
            }

            if(view.classes && _.size(view.classes)){
                properties += ' class="' + view.classes.join(' ') + '"';
            }

            tags += Array(spaces+1).join(' ') + '<' + view.type + properties + '>\n';
            if(view.views){
                tags +=  getTags(view.views, spaces*2);
            }
            tags += Array(spaces+1).join(' ') + '</' + view.type + '>\n';
        });
        return tags;
    };

    $scope.$watch('project.controllers', function(){
        var xml = '<Alloy>\n';

        xml += getTags($scope.getSelectedController().views, 2);

        xml += '</Alloy>'
        editor_view.setValue(xml);

        $scope.buildTSS();
        updatedProject = true;
    }, true);

    $scope.refreshEditor = function(){
        setTimeout(function(){
            editor_view.refresh();
            editor_style.refresh();
        }, 100);
    };

    var getTSS = function(views){
        var tss = '';
        _.each(views, function(view){
            if(view.tss && view.id){
                var tss_value = JSON.stringify(view.tss.value);
                tss += '"' + view.tss.id + '": ';
                tss += tss_value
                        .replace(/\:\{/g, ":{\n")
                        .replace(/\{/g, "{\n  ")
                        .replace(/"\}/g, '"\n}')
                        .replace(/",/g, '",\n  ');
            }
            if(view.views){
                tss += ', \n';
                tss += getTSS(view.views);
            }
        });
        return tss;
    };

    $scope.buildTSS = function(){
        var style = '';
        style += getTSS($scope.getSelectedController().views);
        editor_style.setValue(style);
    };

    $scope.showAddController = function(){
        $scope.newControllerName = '';
        $('#add-controller-modal').modal('show');
        $('#controller_name_modal').focus();
    };

    $scope.createController = function(){
        $scope.newControllerName = $scope.newControllerName.replace(/\s/g, '');

        var new_controller = {
            name: $scope.newControllerName,
            views: [
                {
                    id: $scope.newControllerName + '_window',
                    type: 'Window',
                    selected: true,
                    properties: [{
                        name: 'backgroundColor',
                        value: '#fff'
                    }]
                }
            ]
        };

        $scope.project.controllers.push(new_controller);
        $scope.selectController(new_controller);
        $scope.newControllerName = '';
        $('#add-controller-modal').modal('hide');
    };

    $('body').animate({ opacity: 1 }, 1000);

    $scope.showSave = function() {
        if(!$scope.project._id){
            $('#save-project-modal').modal('show');
        } else {
            $scope.saveProject();
        }
    };

    $scope.saveProject = function(_cb){
        if(!$scope.project._id && !$scope.validId($scope.new.appId)){ return; }

        if(!$scope.project.name){
            if($scope.new.projectName){
                $scope.project.name  = $scope.new.projectName;
                $scope.project.appId = $scope.new.appId;
                $http.post('/api/projects/project', angular.toJson($scope.project)).then(function(response){
                    var project = response.data;
                    $scope.project._id = project._id;
                    socket.emit('project', $scope.project._id);
                    if(_cb){
                        _cb();
                    }
                });
            }
        } else {
            $http.put('/api/projects/project/' + $scope.project._id, angular.toJson($scope.project)).then(function(response){
                if(_cb){
                    _cb();
                }
            });
        }
        $('#save-project-modal').modal('hide');
    };

    $scope.searchProject = function() {
        $scope.getProjects();
        $('#open-project-modal').modal('show');
    };

    $scope.openProject = function(_id){
        $http.get('/api/projects/project/' + _id).then(function(response){
            $scope.project = response.data;
            if($scope.project.public){
                socket.emit('project', $scope.project._id);
            }
        });
        $('#open-project-modal').modal('hide');
    };

    $scope.newProject = function(){
        var new_project = {
            name: '',
            controllers: [
                {
                    name: 'index',
                    selected: true,
                    views   : [
                        {
                            id: 'main',
                            type: 'Window',
                            selected: true,
                            properties: [{
                                name: 'backgroundColor',
                                value: '#bbb'
                            }]
                        }
                    ]
                }
            ]
        };

        if($scope.project._id){
            var _confirm = confirm('Do you want to save your current project?')
            if(_confirm){
                $scope.saveProject(function(){
                    $scope.project = new_project;
                });
            } else {
                $scope.project = new_project;
            }
        }
    }

    $scope.getProjects = function(){
        $http.get('/api/projects').then(function(response){
            $scope.projects = response.data;
        });
    };
    $scope.getProjects();

    $scope.validId = function(id){
        id = id || '';
        return id.match(/^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,13}$/);
    };

    $scope.generate = function(){
        if (!$scope.project._id){ return; }

        $scope.generated      = false;
        $scope.generated_path = '';

        $('#generating-modal').modal('show');

        $scope.saveProject(function(){
            $http.get('/api/projects/generate/' + $scope.project._id).then(function(response){
                $scope.generated      = true;
                $scope.generated_path = '/api/projects/download/' + $scope.project._id;
            });
        });
    };

    $scope.shareSelect2 = {
        id: '_id',
        placeholder: 'Search user',
        minimumInputLength: 1,
        ajax: {
            url: "/api/users/search",
            dataType: 'json',
            data: function (term, page) {
                return {
                    text: term,
                    page_limit: 10
                };
            },
            results: function (data, page) {
                var data = _.filter(data, function(item){ return item._id !== $scope.user._id; });
                return { results: data };
            }
        },
        initSelection: function() {},
        formatResult: function (user) {
            var email = '';
            email = (user.email) ? ' (' + user.email + ')' : '';
            return user.username + email;
        },
        formatSelection: function (user) {
            var email = '';
            email = (user.email) ? ' (' + user.email + ')' : '';
            return user.username + email;
        }
    };

    $scope.shareProject = function() {
        if(!$scope.project._id){ return; }
        $scope.shareUser = false;
        $('#share-modal').modal('show');
    };

    $scope.shareToUser = function() {
        $http.post('/api/projects/share/' + $scope.project._id + '?user='+$scope.shareUser._id).then(function(response){
            console.log(response.data);
        });
        $scope.shareUser = false;
        $('#share-modal').modal('hide');
    };

    var mergeViews = function(local, remote){
        _.each(remote.views, function(remoteView, viewIndex){
            delete remoteView.selected;
            if(!local.views){
                local.views = [];
            }

            if(!local.views[viewIndex]){
                local.views[viewIndex] = remoteView;
            }

            var localView = local.views[viewIndex];
            _.each(remoteView, function(viewProp, keyView){
                if(keyView === 'properties'){
                    _.each(viewProp, function(property, key){
                        if(localView[keyView][key] && property.value && property.value !== localView[keyView][key].value){
                            localView[keyView][key] = property;
                        } else if(!localView[keyView][key]) {
                            localView[keyView][key] = property;
                        }
                    });
                } else {
                    if(keyView === 'views'){
                        mergeViews(localView, remoteView);
                    } else if(keyView !== 'hover' && viewProp !== localView[keyView]) {
                        localView[keyView] = viewProp;
                    }
                }
            });
        });
    };

    socket.on('updateClient', function(project){
        _.each(project.controllers, function(controller, index){
            delete controller.selected;
            delete controller.showRemove;
            if(!$scope.project.controllers[index]){
                $scope.project.controllers.push(controller);
            } else {
                mergeViews($scope.project.controllers[index], controller);
            }
        });
        if(!$scope.$$phase){
            $scope.$apply();
        }
    });

    var updateTimeout;
    $(window).on('keyup', function(){
        if($scope.project.public && updatedProject){
            if(updateTimeout) {
                clearTimeout(updateTimeout);
                updateTimeout = null;
            }
            updateTimeout = setTimeout(function(){
                socket.emit('update', angular.toJson($scope.project));
                updatedProject = false;
            }, 100);
        }
    });

    $(window).on('mouseup', function(){
        if($scope.project.public && updatedProject){
            socket.emit('update', angular.toJson($scope.project));
            updatedProject = false;
        }
    });

    $scope.handleChat = function($event){
        if($event.charCode === 13){
            if(!$('#chat-input').val()) return;
            socket.emit('sendChat', $scope.project._id, { username: $scope.user.username, message: $('#chat-input').val() });
            $scope.chats.push({
                username: $scope.user.username,
                message: $('#chat-input').val()
            });
            $('#chat-input').val('');
            $event.preventDefault();
            setTimeout(function(){
                $("#chat").scrollTop($("#chat")[0].scrollHeight);
            }, 200);
        }
    };

    socket.on('chat', function(chat){
        $scope.chats.push(chat);
        setTimeout(function(){
            $("#chat").scrollTop($("#chat")[0].scrollHeight);
        }, 100);
    });

    window.editor = $scope;

}]);