var socket = io('http://' + location.host);

socket.on('connect', function(){
    socket.emit('connected', user._id);
});

var editor_view = CodeMirror(document.getElementById("editor_view"), {
    mode: "text/html",
    lineNumbers: true,
    matchTags: {bothTags: true},
    extraKeys: {"Ctrl-J": "toMatchingTag"}
});

var editor_style = CodeMirror(document.getElementById("editor_style"), {
    // mode: "application/json",
    lineNumbers: true,
    matchBrackets: true,
    autoCloseBrackets: true,
    mode: "application/ld+json",
    lineWrapping: true
});

app.directive('tielement', function($compile, $timeout, $rootScope) {
    return {
      replace: true,
      restrict: 'E',
      scope: {
        view: '='
      },
      templateUrl: 'templates/tielement.html',
      link: function($scope, element, attrs){
        $scope.getStyle          = $rootScope.getStyle;
        $scope.selectView        = $rootScope.$editorScope.selectView;
        $scope.showAddModal      = $rootScope.$editorScope.showAddModal;
        $scope.removeView        = $rootScope.$editorScope.removeView;
        $scope.getSelectedDevice = $rootScope.$editorScope.getSelectedDevice;

        $scope.enterView = function(view){
            view.hover = true;
        };

        $scope.leaveView = function(view){
            view.hover = false;
        };

        $scope.view.element_id = $scope.view.element_id || new Date().getTime();
        element.attr('id', $scope.view.element_id);

        var parent_element;
        if($scope.view.parent_id){
            parent_element = $('#' + $scope.view.parent_id);
        }

        if($scope.view.type === 'Button'){
            element.addClass('button');
        }

        element.addClass('layout-absolute');
        $scope.$watch('view.properties', function($properties){
            var title = _.find($properties, function(prop){
                prop.name = prop.name || '';
                return prop.name.toLowerCase() === 'title';
            }) || {};

            if(title.value){
                element.html(title.value);
            }

            var layout = _.find($properties, function(prop){
                prop.name = prop.name || '';
                return prop.name.toLowerCase() === 'layout';
            }) || {};

            var top = _.find($properties, function(prop){
                prop.name = prop.name || '';
                return prop.name.toLowerCase() === 'top';
            }) || {};
            top = top.value;

            var left = _.find($properties, function(prop){
                prop.name = prop.name || '';
                return prop.name.toLowerCase() === 'left';
            }) || {};
            left = left.value;

            var bottom = _.find($properties, function(prop){
                prop.name = prop.name || '';
                return prop.name.toLowerCase() === 'bottom';
            }) || {};
            bottom = bottom.value;

            var right = _.find($properties, function(prop){
                prop.name = prop.name || '';
                return prop.name.toLowerCase() === 'right';
            }) || {};
            right = right.value;

            layout = layout.value;

            element
                .removeClass('layout-absolute')
                .removeClass('layout-vertical')
                .removeClass('layout-horizontal');

            if (layout === 'vertical'){
                element.addClass('layout-vertical');
            } else if(layout === 'horizontal'){
                element.addClass('layout-horizontal');
            } else {
                element.addClass('layout-absolute');
            }

            if(parent_element){

                var parent_layout = $rootScope.getView($scope.view.parent_id);
                parent_layout = _.find(parent_layout.properties, function(prop){
                    prop.name = prop.name || '';
                    return prop.name.toLowerCase() === 'layout';
                }) || { value: '' };

                parent_layout = parent_layout.value.toLowerCase();

                if(!parent_layout || parent_layout === 'absolute'){
                    var parent_width  = parent_element.width();
                    var parent_height = parent_element.height();

                    var new_position = {
                        position: 'absolute',
                        top     : parent_height/2 - element.height()/2,
                        left    : parent_width/2 - element.width()/2
                    }

                    if(top){
                        if(!(top+'').match(/px/gi)){
                            top = top * $scope.getSelectedDevice().dpi / 160
                        }
                        new_position.top = top;
                    }

                    if(bottom){
                        if(!(bottom+'').match(/px/gi)){
                            bottom = bottom * $scope.getSelectedDevice().dpi / 160
                        }
                        delete new_position.top;
                        new_position.bottom = bottom;
                    }

                    if(left){
                        if(!(left+'').match(/px/gi)){
                            left = left * $scope.getSelectedDevice().dpi / 160
                        }
                        new_position.left = left;
                    }

                    if(right){
                        if(!(right+'').match(/px/gi)){
                            right = right * $scope.getSelectedDevice().dpi / 160
                        }
                        delete new_position.left;
                        new_position.right = right;
                    }

                    element.css(new_position);
                } else if(parent_layout && parent_layout !== 'absolute') {
                    element.css({
                        position: 'inherit',
                        top: 0,
                        left: 0
                    });
                }
            }
        }, true);

        if($scope.view.views){
            $timeout(function(){
                $('#'+$scope.view.element_id).append($compile('<tielement ng-repeat="view in view.views" view="view"/>')($scope));
            }, 500);
        } else {
            var init = false;
            $scope.$watch('view.views', function(views){
                if(views && !init){
                    $('#'+$scope.view.element_id).append($compile('<tielement ng-repeat="view in view.views" view="view"/>')($scope));
                    init = true;
                }
            });
        }
      }
    };
});