var _ = require('underscore');

// Socket.io events
module.exports = function(socket, users){

    socket.on('connected', function(id){
        users[socket.id]._id = id;
    });

    socket.on('project', function(id){
        users[socket.id].project = id;
    });

    socket.on('disconnect', function(){
        delete users[socket.id];
    });

    socket.on('update', function(project){
        project = JSON.parse(project);

        var _sockets = _.filter(users, function(user){
            return user.project == project._id && user._id != users[socket.id]._id;
        });

        project.last_user = users[socket.id]._id;

        _.each(_sockets, function(_socket){
            _socket.socket.emit('updateClient', project);
        });
    });

    socket.on('sendChat', function(project_id, chat){
        var _sockets = _.filter(users, function(user){
            return user.project == project_id && user._id != users[socket.id]._id;
        });
        _.each(_sockets, function(_socket){
            _socket.socket.emit('chat', chat);
        });
    });

};