// Controllers
var index    = require('./controllers/index');
var users    = require('./controllers/users');
var projects = require('./controllers/projects');

module.exports = function(app){

    var loggedIn = function(req, res, next){
        if(!req.session.user){
            return res.redirect('/login');
        }
        next();
    };

    // Index
    app.get('/', index.index);
    app.get('/login', index.login);

    // Users
    app.post('/api/users/signin', users.login);
    app.post('/api/users/signup', users.create);
    app.get('/api/users/signout', users.signout);
    app.get('/api/users/search', users.search);
    app.post('/api/users/request', users.request);

    // CRUD Projects
    app.get('/api/projects', loggedIn, projects.all);
    app.post('/api/projects/project', loggedIn, projects.create);
    app.get('/api/projects/project/:id', loggedIn, projects.get);
    app.put('/api/projects/project/:id', loggedIn, projects.update);
    app.delete('/api/projects/project/:id', loggedIn, projects.remove);

    // Share
    app.post('/api/projects/share/:id', loggedIn, projects.share);

    // Generate
    app.get('/api/projects/generate/:id', loggedIn, projects.generate);

    // Download
    app.get('/api/projects/download/:id', loggedIn, projects.download);

};