'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
Schema       = mongoose.Schema;


/**
 * Project Schema
 */
var ProjectSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    name: {
        type: String,
        trim: true
    },
    public: {
        default: false,
        type: Boolean
    },
    appId: {
        type: String,
        trim: true
    },
    controllers: {
        type: Object
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    users: {
        type: Array
    }
});

/**
 * Validations
 */
ProjectSchema.path('name').validate(function(name) {
    return name.length;
}, 'Name cannot be blank');

/**
 * Statics
 */
ProjectSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).exec(cb);
};

mongoose.model('Project', ProjectSchema);
