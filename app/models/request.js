'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
Schema       = mongoose.Schema;


/**
 * Request Schema
 */
var RequestSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    email: {
        type: String,
        trim: true
    }
});

/**
 * Statics
 */
RequestSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).exec(cb);
};

mongoose.model('Request', RequestSchema);
