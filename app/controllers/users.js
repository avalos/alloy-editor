'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
User         = mongoose.model('User'),
Request      = mongoose.model('Request');

/**
 * Auth callback
 */
exports.authCallback = function(req, res) {
    res.redirect('/');
};

/**
 * Login form
 */
 exports.signin = function(req, res) {
    res.render('signin');
 };

 exports.request = function(req, res) {
    Request.findOne({ email: req.body.email }, function(err, doc){
        if(!doc){
            var request = new Request({ email: req.body.email });
            request.save();
        }
        res.json({
            success: true
        });
    });
 };

/**
 * Login
 */
exports.login = function(req, res) {
    var username = req.body.username || req.query.username,
    password     = req.body.password || req.query.password;

    if(!username){
        return res.json({
            error: 'Invalid username'
        });
    }

    User.findOne({ username: username }, function(err, doc){

        if(doc){

            if( doc.authenticate(password) ){
                var userData = doc.toObject();
                delete userData.password;
                delete userData.hashed_password;
                delete userData.salt;
                req.session.user = userData;
                res.json({
                    success: true,
                    user: userData
                });
            } else {
                res.json({
                    error: 'Incorrect password'
                });
            }

        } else {
            res.json({
                error: 'User not found'
            });
        }

    });
};

/**
 * Sign up
 */
exports.signup = function(req, res) {
    res.render('signup');
};

/**
 * Logout
 */
exports.signout = function(req, res) {
    delete req.session.user;
    req.session.destroy(function(){
        res.redirect('/');
    });
};

/**
 * Create user
 */
exports.create = function(req, res, next) {
    var user = new User(req.body);
    var message = null;

    user.provider = 'local';
    user.save(function(err) {
        if (err) {
            switch (err.code) {
                case 11000:
                case 11001:
                    message = 'Username already exists';
                    break;
                default:
                    message = 'Please fill all the required fields';
            }

            return res.json({
                error: message
            });
        }

        // req.user = user;
        res.json({
            success: true
        });
    });
};

/**
 * Send User
 */
exports.me = function(req, res) {
    res.json(req.session.user || null);
};

exports.search = function(req, res) {
    var text = req.params.text || req.query.text;
    User.find({ $or: [ { username: { $regex: new RegExp(text, 'gi') } }, { email: { $regex: new RegExp(text, 'gi') } },{ name: { $regex: new RegExp(text, 'gi') } } ] }, 'numusuario name last_name username email profile_image').sort('-created').exec(function(err, users) {
        if (err) {
            res.json({ 'error': 'Error searching user' });
        } else {
            res.json(users);
        }
    });
};

/**
 * Find user by id
 */
exports.user = function(req, res, next, id) {
    User
        .findOne({
            _id: id
        })
        .exec(function(err, user) {
            if (err) return next(err);
            if (!user) return next(new Error('Failed to load User ' + id));
            next();
        });
};