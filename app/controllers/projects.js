'use strict';

var mongoose = require('mongoose'),
Project      = mongoose.model('Project'),
fs           = require('fs'),
_            = require('underscore'),
exec         = require('child_process').exec;

exports.all = function(req, res) {
    Project.find({ $or: [ { user: req.session.user._id }, { users: { $all: req.session.user._id } }] }, function(err, docs){
        res.json(docs);
    });
};

exports.download = function(req, res) {
    res.sendfile('projects/dist/' + req.params.id + '.zip');
};

exports.get = function(req, res) {
    Project.findOne({ _id: req.params.id }, function(err, doc){
        if(doc){
            doc = doc.toJSON();
            if(doc.public && doc.user != req.session.user._id){
                if(_.contains(doc.users, req.session.user._id)){
                    res.json(doc);
                } else {
                    res.json({
                        error: "You don't have permissions to read this project"
                    });
                }

            } else {
                if(doc.user == req.session.user._id){
                    res.json(doc);
                } else {
                    res.json({
                        error: "You don't have permissions to read this project"
                    });
                }
            }
        } else {
            res.json({
                error: 'Project not found'
            });
        }
    });
};

exports.share = function(req, res) {
    Project.findOne({ _id: req.params.id }, function(err, doc){
        var sharedTo = req.query.user;
        var docObject = doc.toJSON();
        if(!docObject.users){
            docObject.users = [];
            docObject.users.push(sharedTo);
        } else {
            if(!_.contains(docObject.users, sharedTo)){
                docObject.users.push(sharedTo);
            }
        }
        docObject.public = true;
        delete docObject._id;
        Project.update({ _id: req.params.id }, docObject, function(err, affected){
            if(err){
                return res.json({
                    error: err.errors
                });
            }
            res.json({
                success: true
            });
        });
    });
};

exports.create = function(req, res) {
    var project = new Project(req.body);
    project.user = req.session.user._id;
    project.save(project);
    res.json({
        _id: project._id,
        success: true
    });
};

exports.update = function(req, res) {
    if(!req.params.id){
        return res.json({
            error: 'You must especify an ID'
        });
    }
    delete req.body._id;
    Project.findOne({ _id: req.params.id }, function(err, doc){
        doc = doc.toJSON();

        if(doc.user == req.session.user._id || (doc.user != req.session.user._id && _.contains(doc.users, req.session.user._id)) ){
            Project.update({ _id: req.params.id }, req.body, function(err, affected){
                if(err){
                    console.error('Error on update', err);
                }
            });
            res.json({
                success: true
            });
        } else {
            res.json({
                error: "You don't have permissions to read this project"
            });
        }

    });
};

exports.remove = function(req, res) {
    if(!req.params.id){
        return res.json({
            error: 'You must especify an ID'
        });
    }
    Project.remove({ _id: req.params.id, user: req.session.user._id });
    res.json({
        success: true
    });
};

var getTSS = function(views){
    var tss = '';
    _.each(views, function(view){
        if(view.tss && view.id){
            var tss_value = JSON.stringify(view.tss.value);
            tss += '"' + view.tss.id + '": ';
            tss += tss_value
                    .replace(/\:\{/g, ":{\n")
                    .replace(/\{/g, "{\n  ")
                    .replace(/"\}/g, '"\n}')
                    .replace(/",/g, '",\n  ');
        }
        if(view.views){
            tss += ', \n';
            tss += getTSS(view.views);
        }
    });
    return tss;
};

var getTags = function(views, spaces){
    var tags = '';
    _.each(views, function(view){
        var properties = '';
        if(view.id){
            properties += ' id="' + view.id + '"';
        } else {
            _.each(view.properties, function(property){
                if(property.value){
                    properties += ' ' + property.name + '="' + property.value + '"';
                }
            });
        }

        if(view.classes && _.size(view.classes)){
            properties += ' class="' + view.classes.join(' ') + '"';
        }

        tags += Array(spaces+1).join(' ') + '<' + view.type + properties + '>\n';
        if(view.views){
            tags +=  getTags(view.views, spaces*2);
        }
        tags += Array(spaces+1).join(' ') + '</' + view.type + '>\n';
    });
    return tags;
};

exports.generate = function(req, res){
    var project_id = req.params.id;

    Project.findOne({ _id: project_id }, function(err, doc){

        if(!doc){
            return res.json({
                error: 'Project not found'
            });
        }

        var project = doc.toJSON();

        // Clean directory
        exec('rm -r projects/src/'+req.params.id, function(err, stdout, stderr){
            var commands = [
                'cd projects/src/',
                'titanium create -n ' + req.params.id + ' --id '+ project.appId.replace(/_/g, '') + ' -p all -u http://github.com/Agnostic -d .',
                'cd ' + req.params.id,
                'alloy new',
            ];

            project.controllers.forEach(function(controller){
                if(controller.name !== 'index'){
                    commands.push('alloy generate controller ' + controller.name);
                }
            });

            exec(commands.join(' && '), function(error, stdout, stderr) {
                if(error){
                    console.log('Generating project', stdout);
                    res.json({
                        error: stderr
                    });
                } else {

                    project.controllers.forEach(function(controller){
                        var style = '';
                        style += getTSS(controller.views);
                        fs.writeFileSync('projects/src/' + project_id + '/app/styles/' + controller.name + '.tss', style);

                        var xml = '<Alloy>\n';
                        xml += getTags(controller.views, 2);
                        xml += '</Alloy>'
                        fs.writeFileSync('projects/src/' + project_id + '/app/views/' + controller.name + '.xml', xml);

                        if(controller.name === 'index'){
                            var _indexController = '// Project generated by Alloy Studio\n\n$.';
                            var _window = _.find(controller.views, function(view){
                                return view.type === 'Window';
                            });
                            if(_window.id){
                                _indexController += _window.id + '.open();';
                            } else {
                                _indexController += 'index.open();';
                            }
                            fs.writeFileSync('projects/src/' + project_id + '/app/controllers/index.js', _indexController);
                        }
                    });

                    // Removing existent dist
                    exec('rm projects/dist/' + project_id + '.zip', function(err, stdout, stderr){
                        // Creating a new zipfile
                        exec('cd projects/src/ && zip -r ../../projects/dist/' + project_id + '.zip ' + project_id + '/', function(err, stdout, stderr){
                            res.json({
                                filename: project_id + '.zip',
                                success: 'Project generated successfully!'
                            });
                        });
                    });
                }
            });
        });

    });
};