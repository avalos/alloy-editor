exports.index = function(req, res){
    if(!req.session.user){
        res.redirect('/login');
    } else {
        res.render('index', {
            user: req.session.user
        });
    }
}

exports.login = function(req, res){
    if(!req.session.user){
        res.render('login');
    } else {
        res.redirect('/');
    }
}