module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    watch: {
      scripts: {
        files: 'public/**/*.js',
        options: {
          livereload: true
        }
        // tasks: ['jshint'],
      },
      html: {
        files: 'public/**/*.html',
        options: {
          livereload: true
        }
        // tasks: ['jshint'],
      },
      css: {
        files: '**/*.css',
        options: {
          livereload: true
        }
      }
    }

  });

  // Load plugin
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['watch']);

};