module.exports = {
    port: 8080,
    db: 'mongodb://localhost/alloystudio',

    // The secret should be set to a non-guessable string that
    // is used to compute a session hash
    sessionSecret: '98sad79a8sd(/&&%$d_)/(ds'
};